import 'package:flutter/material.dart';

final _drawerHeader = UserAccountsDrawerHeader(
  accountName: Text('AVIS'),
  accountEmail: Text('AVIS.feilong.ml'),
  currentAccountPicture: CircleAvatar(
    child: FlutterLogo(
      size: 42,
    ),
  ),
);

final navDrawer = ListView(
  children: <Widget>[
    _drawerHeader,
    ListTile(
      leading: Icon(Icons.bubble_chart),
      title: Text('Plugin'),
    ),
    ListTile(
      leading: Icon(Icons.settings),
      title: Text('Setting'),
    ),
    ListTile(
      leading: Icon(Icons.account_box),
      title: Text('About'),
    ),
  ],
);
