import 'package:flutter/material.dart';
import 'topbar.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'AVIS',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: TopBar(title: 'AVIS'),
    );
  }
}

//class _MyAppState extends State<MyApp> {
//  Brightness brightness;
//
//  @override
//  Widget build(BuildContext context) {
//    return new MaterialApp(
//      title: 'AVIS',
//      theme: new ThemeData(
//        primarySwatch: Colors.blue,
//        brightness: Brightness.dark,
//      ),
//      home: TopBar(title: 'AVIS'),
//    );
//  }
//}
